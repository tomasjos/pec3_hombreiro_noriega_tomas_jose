﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameplayManager : MonoBehaviour
{
   // public BikeController BikeController;
    public MovimientoPersona MovimientoPersona;
    public Text MoneyText;
    public Text PointsText;
    public Text TimeText;
    public Button StartButton;
    public Text Result;
    //public Text RecordText;
    private float initialTime;
    private float recordTime;
    private int secondsToStart = 3;

    private void Start()
    {
        TimeText.text = MoneyText.text = PointsText.text =Result.text=string.Empty;

     /*   BikeController.OnKilled += RestartLevel;
        BikeController.OnReachedEndOfLevel += EndGame;
        BikeController.enabled = false;*/
        MovimientoPersona.OnKilled += RestartLevel;
        MovimientoPersona.OnReachedEndOfLevel += EndGame;
        MovimientoPersona.enabled = false;
      /*  recordTime = PlayerPrefs.GetFloat("recordLevel" + SceneManager.GetActiveScene().buildIndex, 0);

        if (recordTime > 0)
            RecordText.text = "Record: " + recordTime.ToString("00.00") + "s";*/
    }

    private void Update()
    {
        // MovimientoPersona.distanciatierra = 0.5f;
        // Debug.Log(MovimientoPersona.enabled);
        if (MovimientoPersona.enabled)
        {
            TimeText.text = "Time: " + (Time.time - initialTime).ToString("00.00") + "s";
            PointsText.text = "Puntos: " + (MovimientoPersona.puntos).ToString();
            MoneyText.text = "Monedas: " + (MovimientoPersona.monedas).ToString();
            if ((MovimientoPersona.perder == false) &&(MovimientoPersona.ganar==false))
            {
               Result.text = "En juego";
            }
            else if ((MovimientoPersona.perder==true) &&(MovimientoPersona.ganar==false))
            {
                Result.text = "Game Over";
            }
            else if((MovimientoPersona.perder==false)&&(MovimientoPersona.ganar==true))
            {
                Result.text = "Ganaste!";
            }

        }
    /*    if(BikeController.enabled)
        {
            TimeText.text = "Time: " + (Time.time - initialTime).ToString("00.00") + "s";
        }*/
    }

    public void StartGame()
    {
        StartButton.gameObject.SetActive(false);
        Debug.Log("boton");
        TimeText.text = secondsToStart.ToString();
        InvokeRepeating(nameof(Countdown), 1, 1);
    }

    private void Countdown()
    {
        secondsToStart--;
        Debug.Log(secondsToStart);
        if (secondsToStart <= 0)
        {
            Debug.Log("llegue a cero");
            CancelInvoke();
            OnGameStarted();
        }
        else
            TimeText.text = secondsToStart.ToString();
    }

    public void RestartLevel()
    {
        MovimientoPersona.yamurio = false;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    private void OnGameStarted()
    {
        MovimientoPersona.enabled = true;
       // BikeController.enabled = true;
        initialTime = Time.time;
        TimeText.text = string.Empty;
    }

    public void EndGame()
    {
        StartButton.gameObject.SetActive(true);
       // BikeController.enabled = false;
        MovimientoPersona.enabled = false;
        TimeText.text = "FINAL! " + (Time.time - initialTime).ToString("00.00") + "s";

        if ((Time.time - initialTime) < recordTime)
        {
            PlayerPrefs.SetFloat("recordLevel" + SceneManager.GetActiveScene().buildIndex, (Time.time - initialTime));
            TimeText.text = "NEW RECORD! " + (Time.time - initialTime).ToString("00.00") + "s";
        }
        else
        {
            TimeText.text = "FINAL! " + (Time.time - initialTime).ToString("00.00") + "s";
        }
    }
}
