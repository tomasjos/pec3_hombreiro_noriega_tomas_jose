﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cabeza : MonoBehaviour
{
    // Start is called before the first frame update
    GameObject Persona;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.CompareTag("bloquepiedra"))
        {
            if (gameObject.GetComponentInParent<MovimientoPersona>().super == true)
            {
                System.Random random = new System.Random();
                string nombresprite;
                int inicial = random.Next(0, 2);
                coll.gameObject.SetActive(false);
                Debug.Log(inicial);
                if (inicial == 0)
                {
                    Debug.Log("choque con bloque, gano una moneda");
                    gameObject.GetComponentInParent<MovimientoPersona>().monedas+=1;
                    Debug.Log("moneda existe");
                    gameObject.GetComponentInParent<MovimientoPersona>().auxiliar = Instantiate(gameObject.GetComponentInParent<MovimientoPersona>().moneda);
                    nombresprite = "UnJuegoDePlataformas/Sprites/TerrainSuperMario/moneda";
                    Sprite sprites = Resources.Load<Sprite>(nombresprite);
                    gameObject.GetComponentInParent<MovimientoPersona>().auxiliar.transform.tag = "moneda";
                    gameObject.GetComponentInParent<MovimientoPersona>().auxiliar.transform.position = new Vector3(coll.transform.position.x, coll.transform.position.y);
                    gameObject.GetComponentInParent<MovimientoPersona>().auxiliar.SetActive(true);
                    Rigidbody2D rbbloquevacio = gameObject.GetComponentInParent<MovimientoPersona>().auxiliar.AddComponent<Rigidbody2D>() as Rigidbody2D;
                    BoxCollider2D clbloquevacio = gameObject.GetComponentInParent<MovimientoPersona>().auxiliar.AddComponent<BoxCollider2D>() as BoxCollider2D;
                    SpriteRenderer srbloquevacio = gameObject.GetComponentInParent<MovimientoPersona>().auxiliar.AddComponent<SpriteRenderer>() as SpriteRenderer;
                    srbloquevacio.sprite = sprites;
                    Debug.Log(sprites);
                    gameObject.GetComponentInParent<MovimientoPersona>().auxiliar.GetComponent<Rigidbody2D>().position = gameObject.GetComponentInParent<MovimientoPersona>().auxiliar.transform.position;
                    gameObject.GetComponentInParent<MovimientoPersona>().auxiliar.GetComponent<BoxCollider2D>().isTrigger = false;
                    //  moneda.GetComponent<BoxCollider2D>().offset.Set(0, 0);
                    //  moneda.GetComponent<BoxCollider2D>().size.Set((float)0.08, (float)0.48);
                    gameObject.GetComponentInParent<MovimientoPersona>().auxiliar.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 300));
                    MovimientoBolsa movbol = gameObject.GetComponentInParent<MovimientoPersona>().gameObject.GetComponentInParent<MovimientoPersona>().auxiliar.AddComponent<MovimientoBolsa>() as MovimientoBolsa;

                    gameObject.GetComponentInParent<MovimientoPersona>().auxiliar.layer = 8;
                    gameObject.GetComponentInParent<MovimientoPersona>().auxiliar.GetComponent<SpriteRenderer>().sortingOrder = 1;
                    // Invoke("Destruirmoneda", 2f);
                    // Destroy(moneda);
                }
                else if (inicial == 1)
                {
                    Debug.Log("choque con bloque, gano 100 puntos");
                    gameObject.GetComponentInParent<MovimientoPersona>().puntos += 100;
                    gameObject.GetComponentInParent<MovimientoPersona>().auxiliar = Instantiate(gameObject.GetComponentInParent<MovimientoPersona>().moneda);
                    nombresprite = "UnJuegoDePlataformas/Sprites/TerrainSuperMario/cienpuntos";
                    Sprite sprites = Resources.Load<Sprite>(nombresprite);
                    gameObject.GetComponentInParent<MovimientoPersona>().auxiliar.transform.tag = "powerup";
                    gameObject.GetComponentInParent<MovimientoPersona>().auxiliar.transform.position = new Vector3(coll.transform.position.x, coll.transform.position.y);
                    gameObject.GetComponentInParent<MovimientoPersona>().auxiliar.SetActive(true);
                    Rigidbody2D rbbloquevacio = gameObject.GetComponentInParent<MovimientoPersona>().auxiliar.AddComponent<Rigidbody2D>() as Rigidbody2D;
                    BoxCollider2D clbloquevacio = gameObject.GetComponentInParent<MovimientoPersona>().auxiliar.AddComponent<BoxCollider2D>() as BoxCollider2D;
                    SpriteRenderer srbloquevacio = gameObject.GetComponentInParent<MovimientoPersona>().auxiliar.AddComponent<SpriteRenderer>() as SpriteRenderer;
                    srbloquevacio.sprite = sprites;
                    Debug.Log(sprites);

                    gameObject.GetComponentInParent<MovimientoPersona>().auxiliar.GetComponent<Rigidbody2D>().position = gameObject.GetComponentInParent<MovimientoPersona>().auxiliar.transform.position;
                    gameObject.GetComponentInParent<MovimientoPersona>().auxiliar.GetComponent<BoxCollider2D>().isTrigger = false;
                    //  moneda.GetComponent<BoxCollider2D>().offset.Set(0, 0);
                    //  moneda.GetComponent<BoxCollider2D>().size.Set((float)0.08, (float)0.48);
                    gameObject.GetComponentInParent<MovimientoPersona>().auxiliar.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 100));
                    MovimientoBolsa movbol = gameObject.GetComponentInParent<MovimientoPersona>().auxiliar.AddComponent<MovimientoBolsa>() as MovimientoBolsa;

                    gameObject.GetComponentInParent<MovimientoPersona>().auxiliar.layer = 8;
                    gameObject.GetComponentInParent<MovimientoPersona>().auxiliar.GetComponent<SpriteRenderer>().sortingOrder = 1;

                    // Invoke("Destruirmoneda", 2f);
                }
            }
        }
    }
}
