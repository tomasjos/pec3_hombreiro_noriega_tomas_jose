﻿using System;
using System.Collections;            
using System.Collections.Generic;
using UnityEngine;

public class MovimientoPersona : MonoBehaviour
{
    public float LinearSpeed = 10;
    public float JumpLevel = 300;
    public int puntos=0;
    public int monedas = 0;
    public bool super = false;
   // public Transform bloquevacio;
   [SerializeField]
    public GameObject moneda;
    [SerializeField]

    public GameObject auxiliar;
    public Action OnKilled;
    public Action OnReachedEndOfLevel;
    public GameObject proyectil;
    public bool saltar=true;
    public bool perder = false;
    public bool ganar = false;
    private bool corre = false;
    private bool parado = true;
    private bool salta = false;
    private bool muerto = false;
    public bool yamurio = false;
    public Vector3 posicionproyectil;
    public Transform suelo;
    public float distanciatierra = 0.5f;
    public LayerMask estierra;
    private Rigidbody2D rb;
    private Sprite avatar;
    private int contactos = 0;
    private float Radius;    // Start is called before the first frame update
    private Animator animator;
    private void Start()
    {
        GetComponent<Rigidbody2D>().centerOfMass = new Vector2((float)0.0, (float)1.5);
        animator = GetComponent<Animator>();
        Animate();
       
    }
    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        suelo = GameObject.Find("Suelo").transform;
    }
    private void FixedUpdate()
    {
     
       
    }
    void Animate()
    {
        animator.SetBool("corre", corre);
        animator.SetBool("parado",parado);
        animator.SetBool("salta", salta);
        animator.SetBool("muerto", muerto);
    }
    // Update is called once per frame
    void Update()
    {
        saltar = Physics2D.OverlapCircle(suelo.position, distanciatierra, estierra);
        if (perder==false)
        {
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                corre = true;
                parado = false;
                salta = false;
                muerto = false;
                Animate();
                MoveBackward();
               /* if (Input.GetKey(KeyCode.UpArrow))
                {
                    GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Sprites/SuperMario/Jump");
                   
                    if (saltar == true)
                    {
                        RotateRight();
                        MoveBackward();
                    }
                    else
                    {

                    }
                }
                else
                {
                    GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Sprites/SuperMario/Run");
                    MoveBackward();
                }*/
            }
            if (Input.GetKey(KeyCode.RightArrow))
            {
                corre = true;
                parado = false;
                salta = false;
                muerto = false;
                Animate();
                MoveForward();
               /* if (Input.GetKey(KeyCode.UpArrow))
                {
                    GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Sprites/SuperMario/Jump");
                    
                    if (saltar==true)
                    {
                        RotateLeft();
                        MoveForward();
                    }
                   // MoveForward();
                }
                else
                {
                    GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Sprites/SuperMario/Run");
                    MoveForward();
                }*/

            }
            if ((saltar == true) && (Input.GetKeyDown(KeyCode.UpArrow)))
            {
                corre = false;
                muerto = false;
                salta = true;
                parado = false;
                Animate();
                Jump();
            }
            if ((!Input.GetKey(KeyCode.RightArrow) &&(!Input.GetKey(KeyCode.LeftArrow))&&(!Input.GetKey(KeyCode.UpArrow))))
            {
                // GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Sprites/SuperMario/Persona");
                corre = false;
                parado = true;
                salta = false;
                muerto = false;
                Animate();
            }
              if (Input.GetKey(KeyCode.DownArrow))
              {
                auxiliar=Instantiate(proyectil, new Vector3((float)0.5 + gameObject.transform.position.x, gameObject.transform.position.y - (float)0.1),Quaternion.identity);
 
                //  posicionproyectil = gameObject.transform.position;
                //  posicionproyectil.x = posicionproyectil.x + posicionproyectil.x / 2;
              /*   auxiliar=Instantiate(moneda);

               String  nombresprite = "Sprites/TerrainSuperMario/spr_missile_half";
                   Sprite sprites = Resources.Load<Sprite>(nombresprite);
                auxiliar.transform.tag = "misil";
               auxiliar.transform.position = new Vector3((float)0.5+gameObject.transform.position.x, gameObject.transform.position.y- (float)0.1);
                auxiliar.SetActive(true);
               Rigidbody2D rbbloquevacio = auxiliar.AddComponent<Rigidbody2D>() as Rigidbody2D;
                CapsuleCollider2D clbloquevacio = auxiliar.AddComponent<CapsuleCollider2D>() as CapsuleCollider2D;
                SpriteRenderer srbloquevacio = auxiliar.AddComponent<SpriteRenderer>() as SpriteRenderer;
                srbloquevacio.sprite = sprites;
                Debug.Log(sprites);
                auxiliar.GetComponent<Rigidbody2D>().position = auxiliar.transform.position;
                auxiliar.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Kinematic;
                
                auxiliar.GetComponent<CapsuleCollider2D>().isTrigger = false;
                auxiliar.GetComponent<CapsuleCollider2D>().size.Set((float)0.36, (float)0.16);
                auxiliar.GetComponent<CapsuleCollider2D>().direction = CapsuleDirection2D.Horizontal;
                //  moneda.GetComponent<BoxCollider2D>().offset.Set(0, 0);
                //  moneda.GetComponent<BoxCollider2D>().size.Set((float)0.08, (float)0.48);
                
                Controlproyectil movpro = auxiliar.AddComponent<Controlproyectil>() as Controlproyectil;*/
              }
            // GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("UnJuegoDePlataformas/Sprites/SuperMario/Persona");
        }
        else
        {
            corre = false;
            parado = false;
            salta = false;
            muerto = true;
            if (yamurio==false)
            {
                yamurio = true;
                Animate();
            }
            
        }
        
       /* else
        {
           Stop();
        }
        */
    }
    
    private void MoveForward()
    {
        if (IsGrounded())
        {
            
            var right = transform.right;
            rb.velocity += new Vector2(right.x * LinearSpeed, right.y ) * Time.deltaTime;
       }
    }
    private void RotateLeft()
    {
        if (IsGrounded())
        {
            var right = transform.right;
            rb.SetRotation(-10);

        }
    }
    private void RotateRight()
    {
        if (IsGrounded())
        {
            var right = transform.right;
            rb.SetRotation(10);

        }
    }
    private void MoveBackward()
    {
         if (IsGrounded())
          {
      //  GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Sprites/SuperMario/Run");
           
            var right = transform.right;
            rb.velocity -= new Vector2(right.x * LinearSpeed, right.y ) * Time.deltaTime;
        }
    }
    private void Jump()
    {
       // GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Sprites/SuperMario/Jump");
        var right = transform.right;
        // transform.Rotate(new Vector2(0, 1) * Time.deltaTime * LinearSpeed, Space.World);
       
                    rb.AddForce (new Vector2(0, JumpLevel));
            
       
           
    }
    private void Down()
    {
        if (!IsGrounded())
        {
            var right = transform.up;
         //   transform.Rotate(new Vector2(0, -1) * Time.deltaTime * LinearSpeed, Space.World);
            rb.velocity = new Vector2(0, right.y * LinearSpeed) * Time.deltaTime;
        }
    }
    private void Stop()
    {
        if (IsGrounded())
        {
            var right = transform.right;
            rb.velocity += new Vector2(0, 0) * Time.deltaTime;
        }
    }
    private void OnCollisionEnter2D(Collision2D coll)
    {
        //Debug.Log("Physics collision!");
       
        if (coll.gameObject.CompareTag("bloqueinterrogacion"))
        {
           
           // Destroy(coll.gameObject);
            String nombresprite = "Sprites/TerrainSuperMario/bloquevacio";
            coll.gameObject.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>(nombresprite);
            coll.gameObject.GetComponent<Transform>().tag = "bloquevacio";
            Debug.Log(moneda);
            System.Random random = new System.Random();
           
            int inicial = random.Next(0, 3);
            Debug.Log(inicial);
            if (inicial==0)
            {
                Debug.Log("choque con bloque, gano una moneda");
                monedas = monedas + 1;
                Debug.Log("moneda existe");
                auxiliar = Instantiate(moneda);
                nombresprite = "Sprites/TerrainSuperMario/moneda";
                Sprite sprites = Resources.Load<Sprite>(nombresprite);
                auxiliar.transform.tag = "moneda";
                auxiliar.transform.position = new Vector3(coll.transform.position.x, coll.transform.position.y);
                auxiliar.SetActive(true);
                Rigidbody2D rbbloquevacio = auxiliar.AddComponent<Rigidbody2D>() as Rigidbody2D;
                BoxCollider2D clbloquevacio = auxiliar.AddComponent<BoxCollider2D>() as BoxCollider2D;
                SpriteRenderer srbloquevacio = auxiliar.AddComponent<SpriteRenderer>() as SpriteRenderer;
                srbloquevacio.sprite = sprites;
                Debug.Log(sprites);
                auxiliar.GetComponent<Rigidbody2D>().position = auxiliar.transform.position;
                auxiliar.GetComponent<Rigidbody2D>().mass = 1;
                auxiliar.GetComponent<Rigidbody2D>().gravityScale = 0;
                auxiliar.GetComponent<BoxCollider2D>().isTrigger = false;
                //  moneda.GetComponent<BoxCollider2D>().offset.Set(0, 0);
                //  moneda.GetComponent<BoxCollider2D>().size.Set((float)0.08, (float)0.48);
                auxiliar.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 300));
                MovimientoBolsa movbol = auxiliar.AddComponent<MovimientoBolsa>() as MovimientoBolsa;

                auxiliar.layer = 8;
                auxiliar.GetComponent<SpriteRenderer>().sortingOrder = 1;
               // Invoke("Destruirmoneda", 2f);
                // Destroy(moneda);
            }
            else if (inicial==1)
            {
                Debug.Log("choque con bloque, gano 100 puntos");
                puntos = puntos + 100;
                auxiliar = Instantiate(moneda);
                nombresprite = "Sprites/TerrainSuperMario/cienpuntos";
                Sprite sprites = Resources.Load<Sprite>(nombresprite);
                auxiliar.transform.tag = "powerup";
                auxiliar.transform.position = new Vector3(coll.transform.position.x, coll.transform.position.y);
                auxiliar.SetActive(true);
                Rigidbody2D rbbloquevacio = auxiliar.AddComponent<Rigidbody2D>() as Rigidbody2D;
               // BoxCollider2D clbloquevacio = auxiliar.AddComponent<BoxCollider2D>() as BoxCollider2D;
                SpriteRenderer srbloquevacio = auxiliar.AddComponent<SpriteRenderer>() as SpriteRenderer;
                srbloquevacio.sprite = sprites;
                Debug.Log(sprites);
                
                auxiliar.GetComponent<Rigidbody2D>().position = auxiliar.transform.position;
                auxiliar.GetComponent<Rigidbody2D>().mass = 1;
                auxiliar.GetComponent<Rigidbody2D>().gravityScale = 0;
               // auxiliar.GetComponent<BoxCollider2D>().isTrigger = false;
                //  moneda.GetComponent<BoxCollider2D>().offset.Set(0, 0);
                //  moneda.GetComponent<BoxCollider2D>().size.Set((float)0.08, (float)0.48);
                auxiliar.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 100));
                MovimientoBolsa movbol = auxiliar.AddComponent<MovimientoBolsa>() as MovimientoBolsa;

                auxiliar.layer = 8;
                auxiliar.GetComponent<SpriteRenderer>().sortingOrder = 1;

               // Invoke("Destruirmoneda", 2f);
            }
            else if (inicial == 2)
            {
                Debug.Log("choque con bloque, basa a nivel Super");
                super = true;
                Invoke("nivelnormal", 10f);
                auxiliar = Instantiate(moneda);
                nombresprite = "Sprites/TerrainSuperMario/superseta";
                Sprite sprites = Resources.Load<Sprite>(nombresprite);
                auxiliar.transform.tag = "superseta";
                auxiliar.transform.position = new Vector3(coll.transform.position.x, coll.transform.position.y);
                auxiliar.SetActive(true);
                gameObject.transform.localScale.Set((float)0.25, (float)0.25,(float) 0.25);
                gameObject.GetComponent<Transform>().localScale = new Vector3((float)0.25,(float)0.25,(float)0.25);
                Rigidbody2D rbbloquevacio = auxiliar.AddComponent<Rigidbody2D>() as Rigidbody2D;
               // BoxCollider2D clbloquevacio = auxiliar.AddComponent<BoxCollider2D>() as BoxCollider2D;
                SpriteRenderer srbloquevacio = auxiliar.AddComponent<SpriteRenderer>() as SpriteRenderer;
                srbloquevacio.sprite = sprites;
                Debug.Log(sprites);

                auxiliar.GetComponent<Rigidbody2D>().position = auxiliar.transform.position;
               // auxiliar.GetComponent<BoxCollider2D>().isTrigger = false;
                auxiliar.GetComponent<Rigidbody2D>().mass = 1;
                auxiliar.GetComponent<Rigidbody2D>().gravityScale = 1;
                //  moneda.GetComponent<BoxCollider2D>().offset.Set(0, 0);
                //  moneda.GetComponent<BoxCollider2D>().size.Set((float)0.08, (float)0.48);
                auxiliar.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 200));
                MovimientoBolsa movbol = auxiliar.AddComponent<MovimientoBolsa>() as MovimientoBolsa;

                auxiliar.layer = 8;
                auxiliar.GetComponent<SpriteRenderer>().sortingOrder = 1;

                // Invoke("Destruirmoneda", 2f);
            }


        }
      
        else if((coll.gameObject.CompareTag("seta"))&&(super==false))
        {
            Debug.Log("Perdiste");
            corre = false;
            parado = false;
            salta = false;
            muerto = true;
            Animate();
           // GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Sprites/SuperMario/Dead");
            //transform.Rotate(new Vector3(0,0,1),-90);
            perder = true;
            Invoke("empiezadesdeposicion", 2f);

           
        }
        else if(coll.gameObject.CompareTag("GameOver"))
        {
            corre = false;
            parado = false;
            salta = false;
            muerto = true;
            //Animate();
           // GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Sprites/SuperMario/Dead");
            Debug.Log("Perdiste");
            perder = true;
            OnKilled?.Invoke();
        }
        else if(coll.gameObject.CompareTag("EndOfLevel"))
        {
            Debug.Log("ganaste");
            ganar = true;
            GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Sprites/SuperMario/Persona");
            OnReachedEndOfLevel?.Invoke();
        }
        
    }

    private void Destruirmoneda()
    {
        Debug.Log("objeto moneda/puntos/super destruido");
        Destroy(auxiliar);
    }
    private void nivelnormal()
    {
        Debug.Log("vuelve a nivel normal");
        super = false;
        gameObject.GetComponent<Transform>().localScale = new Vector3((float)0.187588, (float)0.187588, (float)0.187588);

    }
    private bool IsGrounded()
    {
        // return Physics2D.OverlapCircleAll(suelo.position,(float)0.1).Length >= 1;
        return Physics2D.OverlapCircle(suelo.position, distanciatierra, estierra);
    }
/*
    private void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log("Trigger collision!");

        if (other.gameObject.CompareTag("EndOfLevel"))
        {
            OnReachedEndOfLevel?.Invoke();

        }
        else if ((!other.gameObject.CompareTag("Suelo"))&&(!other.gameObject.CompareTag("bloquepiedra")))
        {
           saltar = false;
        }
        else if (other.gameObject.CompareTag("bloquepiedra"))
        {

        }
        else if(other.gameObject.CompareTag("GameOver"))
        {
            Debug.Log("entre en el bloque de killed");
            OnKilled?.Invoke();
        }
    }*/
    void empiezadesdeposicion()
    {
        corre = false;
        salta = false;
        parado = true;
        muerto = false;
        transform.position = new Vector3(transform.position.x + (float)0.2,transform.position.y);
        Animate();
        perder = false;
        yamurio = false;
    }
}


