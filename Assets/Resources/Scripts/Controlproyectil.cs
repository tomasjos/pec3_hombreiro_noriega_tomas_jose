﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controlproyectil : MonoBehaviour
{
    // Start is called before the first frame update
    public float speedPro;
    public float velocidadPro;
    public GameObject explosion;
    public GameObject auxiliar;
    float x;
    ParticleSystem.ColorOverLifetimeModule colorModule;
    Gradient ourGradient;
    void Start()
    {
        x = gameObject.transform.position.x;
        speedPro = 10f;
    }
    private void Awake()
    {
        speedPro = 10f;
    }
    // Update is called once per frame
    void Update()
    {
        velocidadPro = speedPro * Time.deltaTime;
        gameObject.transform.Translate(Vector3.right * velocidadPro);
        if (gameObject.transform.position.x-x>4f)
        {
            Destroy(gameObject);
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.CompareTag("seta") || (collision.gameObject.CompareTag("bloquepiedra")))
        {
            Debug.Log("explosion");
            explosion = Instantiate(explosion,gameObject.transform.position, explosion.transform.rotation) as GameObject;
            Destroy(explosion.gameObject, 1.0f);
            Debug.Log("explosion despues");
             Destroy(collision.gameObject);


        }
        Destroy(gameObject);

    }
    
    
}
