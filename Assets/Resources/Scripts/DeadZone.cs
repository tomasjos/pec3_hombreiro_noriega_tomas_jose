﻿using UnityEngine;

public class DeadZone : MonoBehaviour
{
    public GameplayManager GameplayManager;
    string result = "";
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if ((collision.CompareTag("Persona")))
        {
            result="Game Over";
            GameplayManager.Result.text = result;
            GameplayManager.RestartLevel();
        }
    }
}
