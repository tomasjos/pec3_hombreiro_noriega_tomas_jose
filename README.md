PEC2_HombreiroNoriega_TomásJose
El proyecto de juego de artilleria es un ejemplo de como emplear las herramientas de unity 2d para crear un nivel de juego imitando el conocido juego supermario.

Para crear el juego se ha aprovechado la plantilla de la PEC2 entregada por el profesor y el proyecto PEC2 entregado.

**Desarrollo correspondiente a la PEC2**

Se ha añadido una serie de sprites que se han obtenido de la página indicada por el profesor, y que se han empleado para crear el suelo y las plataformas, así como los 
bloques de piedra, los de interrogación y los vacios, así como el personaje  del que se han usado diferentes sprites para el salto, la carrera o la eliminación, 
y también sprites de edificio, cajas, arbustos, setas y supersetas -setas naranjas- 
Se han creado asimismo dos sprites, uno para monedas y otro para puntos. 

En cuanto al código del juego, se han creado una serie de scripts que realizan las tareas indicadas por el enunciado
movimientopersona.cs es el script que crea el objeto personaje, implementa los movimientos hacia adelante -tecla cursor derecha-, hacia atras -tecla 
cursor izquierda y salto -tecla cursor hacia arriba-, así como las funciones de colision
en las que se define la acción que se produce cuando el personaje impacta con un bloque interrogación, un bloque de piedra, una seta,  un deadzone 
o el fin del nivel. Se ha usado una funcion random para generar aleatoriamente el premio que sale al impactar con los bloques -monedas, powerups y supermushroom-. Posteriormente se crea el objeto correspondiente en 
el escenario, y se programa una duración de varios segundos de visibilidad para los premios. En el caso del supermushroom la duración de la energia extra es de 
diez segundo.

bikefollower.cs es el script original en el que se acoplaba la camara al personaje para que le siga
deadzone.cs, seta.cs, edificio.cs son scripts que inician los respectivos objetos y generan las funciones de colision correspondientes.
movimientobolsa.cs es el script asociado a la moneda/powerup/supermushroom, que implementa la función colision
gameplaymanager.cs gestiona el inicio y fin del nivel , su reinicio así como la información sobre el juego que debe aparecer -tiempo, puntos, monedas, situacion
del juego-.

Se han creado materiales nuevos -friccionl-para aumentar el rozamiento y se ha cambiado el centro de gravedad del personaje.

**Desarrollo correspondiente a la PEC3**

Se han creado cuatro animaciones, run, dead, idle y jump, correspondientes a los cuatro estados posibles del personaje. En MovimientoPersona.cs se ha implementado 
un metodo Animate() que llama al objeto Animator de la siguiente forma
void Animate()
    {
        animator.SetBool("corre", corre);
        animator.SetBool("parado",parado);
        animator.SetBool("salta", salta);
        animator.SetBool("muerto", muerto);
    }
    
La maquina de estados tiene los cuatro estados indicados. El estado dead se alcanza a partir de Any. los otros tres se interconectan entre ellos 
de modo que se puede saltar de uno a otro en cuanto se pulsan las teclas de avance, salto o se deja de pulsar teclas.Se han creado las transiciones
correspondientes entre los estados.

En cuanto a los proyectiles, se ha creado un prefab proyectil y un script controlproyectil.cs. Se dispara el proyectil con el cursor "abajo". Se ha establecido que cuando la distancia recorrida por el proyectil
es 4f se autodestruya. En controlproyectil se establece que la colision con una seta provoca su destruccion. La explosion correspondiente se consigue con un prefab
explosion, creado con un gameObject al  que se le añade un particle system. Una vez que se produce la colision se instancia una explosion y se destruye la seta
, el misil y la propia explosión -esta última a los dos frames. La activacion de los proyectiles se hace desde movimientopersona.cs
Se han utilizado tag para comprobar con que objetos colisiona el misil, así como el personaje, de cara a activar los premios, las muertes y los reinicios de nivel.
Se crea de nuevo el personaje al reiniciar el nivel en la posición inicial, lo que ocurre cuando cae en una deadzone.Cuando lo mata una seta se reinicia en la posición en la que murio.
En cuanto a la IA de los enemigos, se ha implementado un sistema sencillo por el cual se desplazan a derecha e izquierda una distancia maxima de 1.2f


